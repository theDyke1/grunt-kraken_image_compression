/*
 * grunt-kraken_image_compression
 * 
 *
 * Copyright (c) 2016 
 * Licensed under the MIT license.
 */

'use strict';

var fs = require('fs'),
  os = require('os'),
  async = require('async'),
  Kraken = require('kraken'),
  request = require('request'),
  chalk = require('chalk'),
  pretty = require('pretty-bytes');


module.exports = function(grunt) {
  grunt.registerMultiTask('kraken_image_compression', 'Compress images using Kraken', function() {
    
    var isJPG = function(src){
     return /.jpg/.test(src);
    };

    var isGIF = function(src){
      return /.gif/.test(src);
    };

    var isPNG = function(src){
      return /.png/.test(src);
    };

    var done = this.async(),
      files = this.files,
      options = this.options({
        lossy: false
      });

    var total = {
      bytes: 0,
      kraked: 0,
      files: 0
    };

    async.forEachLimit(files, os.cpus().length, function(file, next) {
      var isSupported = false;
      //set default optoins if not set in config or command line
      options = (function() {
          if (options.fileType == null) {
              options.fileType = ['jpg', 'png'];
          }
          if (options.compressionType == null) {
              options.compressionType = {};
              options.compressionType.png = 'losssless';
              options.compressionType.jpg = 'lossy';
              options.compressionType.gif = 'losssless';
          }
          if (options.preserve_meta == null) {
              options.preserve_meta = ["profile", "date", "copyright", "geotag", "orientation"];
          }
          if (options.quality == null) {
              options.quality = {};
              options.quality.png = '100';
              options.quality.jpg = '80';
              options.quality.gif = '100';
          }
          if (options.subsampling == null) {
              options.subsampling = {};
              options.subsampling.png = '4:4:4';
              options.subsampling.jpg = '4:4:4';
              options.subsampling.gif = '4:4:4';
          }
          return options;
      }());

      options.fileType.forEach(function(type){
        var str = file.src[0];
        var patt = new RegExp(type);
        var res = patt.test(str);
        if(res){
           isSupported = true;
        }
      });

      var lossy = function(){
        var re = /([^.]*$)/g; 
        var res = re.exec(file.src[0]);
        res = options.compressionType[res[1]];
        return (res==='lossy')?true:false;
      };

      var samp_scheme = function(file){
        var re = /([^.]*$)/g; 
        var res = re.exec(file)[1];
        res = options.subsampling[res];
        return res;
      };

      var qual = function(file){
        var re = /([^.]*$)/g; 
        var res = re.exec(file)[1];

        res = parseInt(options.quality[res]);
        return res;
      };
      
      if (!isSupported) {
        console.log('Skipping unsupported image ' + file.src[0]);
        return next();
      }

      var kraken = new Kraken({
        api_key: options.key,
        api_secret: options.secret
      });

      var opts = {
        file: file.src[0],
        lossy: lossy(),
        wait: true,
        preserve_meta: options.preserve_meta,
        sampling_scheme: samp_scheme(file.src[0]),
        quality: qual(file.src[0])
      };
     
      function directoryPath(path) {
        var str = path;
        var res = /.+\//.exec(str);
        return res[0];
      }

      function checkDirectorySync(directory) {

        try {
          fs.statSync(directory);

        } catch (e) {
          console.log(chalk.green('✔ ') + 'created destination directory:' + chalk.magenta(directory));
          fs.mkdirSync(directory);
        }
      }
   
      kraken.upload(opts, function(data) {

        if (!data.success) {
          grunt.warn('Error in file ' + file.src[0] + ': ' + data.message || data.error);
          return next();
        }

        var originalSize = data.original_size,
          krakedSize = data.kraked_size,
          savings = data.saved_bytes;

        var percent = (((savings) * 100) / originalSize).toFixed(2),
          savedMsg = 'saved ' + pretty(savings) + ' - ' + percent + '%',
          msg = savings > 0 ? savedMsg : 'already optimized';

        total.bytes += originalSize;
        total.kraked += krakedSize;
        total.files++;

        var directory = directoryPath(file.dest);

        checkDirectorySync(directory);
        request(data.kraked_url, function(err) {

          if (err) {
            grunt.warn(err + ' in file ' + file.src[0]);
            return next();
          }

          grunt.log.writeln(chalk.green('✔ ') + file.src[0] + chalk.gray(' (' + msg + ')'));
          process.nextTick(next);

        }).pipe(fs.createWriteStream(file.dest));
      });
    }, function(err) {
      if (err) {
        grunt.warn(err);
      }

      var percent = (((total.bytes - total.kraked) * 100) / total.bytes).toFixed(2);
      var savings = total.bytes - total.kraked;
      var msg = 'All done. Kraked ' + total.files + ' image';

      msg += total.files === 1 ? '' : 's';
      msg += chalk.gray(' (saved ' + pretty(savings) + ' - ' + percent + '%)');

      grunt.log.writeln(msg);
      done();

    });
  });
};