# grunt-kraken_image_compression

> kraken api support

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-kraken_image_compression --save
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-kraken_image_compression');
```

## The "kraken_image_compression" task

### Overview
In your project's Gruntfile, add a section named `kraken_image_compression` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
    // Configuration to be run.
    kraken_image_compression: {
        options: {
                key: 'abc',
                secret: 'abc',
                quality: {
                    png: grunt.option('png_quality') || '100',
                    jpg: grunt.option('jpg_quality') || '80',
                    gif: grunt.option('gif_quality') || '100'
                },
                subsampling: {
                    png: grunt.option('png_subsampling') || '4:4:4',
                    jpg: grunt.option('jpg_subsampling') || '4:4:4',
                    gif: grunt.option('gif_subsampling') || '4:4:4'
                },
                preserve_meta: ["profile", "date", "copyright", "geotag", "orientation"],
                fileType: ['jpg', 'png'],
                compressionType: {
                    png: grunt.option('png_compression') || 'losssless',
                    jpg: grunt.option('jpg_compression') || 'lossy',
                    gif: grunt.option('gif_compression') || 'lossless'
                }
        },
        dynamic: {
            files: [{
                expand: true,
                cwd: 'images/', //specify source folder here, all subfolders within source folder will be kraked and created in destination folder.
                src: ['**/*.{png,jpg,jpeg,gif}'], //file extensions for kraking
                dest: 'imagesKraked/' //desitination folder
            }]
        }
    }
  });
```

### Options

#### options.separator
Type: `String`
Default value: `',  '`

A string value that is used to do something with whatever.

#### options.punctuation
Type: `String`
Default value: `'.'`

A string value that is used to do something else with whatever else.

### Usage Examples

#### Default Options
You need to specify your source folder where your images are located.  Any subfolders will also be scanned for images and then compressed using the kraken api. 

#### New options added
You can alter options in the config file like below.  Also command line options are available eg grunt kraken_image_compression --jpg_quality=60 would set quality to 60. 

```js
grunt.initConfig({
    // Configuration to be run.
    kraken_image_compression: {
        options: {
                key: 'abc',
                secret: 'abc',
                quality: {
                    png: grunt.option('png_quality') || '100',
                    jpg: grunt.option('jpg_quality') || '80',
                    gif: grunt.option('gif_quality') || '100'
                },
                subsampling: {
                    png: grunt.option('png_subsampling') || '4:4:4',
                    jpg: grunt.option('jpg_subsampling') || '4:4:4',
                    gif: grunt.option('gif_subsampling') || '4:4:4'
                },
                preserve_meta: ["profile", "date", "copyright", "geotag", "orientation"],
                fileType: ['jpg', 'png'],
                compressionType: {
                    png: grunt.option('png_compression') || 'losssless',
                    jpg: grunt.option('jpg_compression') || 'lossy',
                    gif: grunt.option('gif_compression') || 'lossless'
                }
        },
        dynamic: {
            files: [{
                expand: true,
                cwd: 'images/', //specify source folder here, all subfolders within source folder will be kraked and created in destination folder.
                src: ['**/*.{png,jpg,jpeg,gif}'], //file extensions for kraking
                dest: 'imagesKraked/' //desitination folder
            }]
        }
    }
  });
```

if using load-grunt-config then save file as kraken_image_compression.js with something like the following inside:


```js
module.exports = function(grunt) {
    return {
        kraken_image_compression: {
            options: {
                key: 'abc',
                secret: 'abc',
                quality: {
                    png: grunt.option('png_quality') || '100',
                    jpg: grunt.option('jpg_quality') || '80',
                    gif: grunt.option('gif_quality') || '100'
                },
                subsampling: {
                    png: grunt.option('png_subsampling') || '4:4:4',
                    jpg: grunt.option('jpg_subsampling') || '4:4:4',
                    gif: grunt.option('gif_subsampling') || '4:4:4'
                },
                preserve_meta: ["profile", "date", "copyright", "geotag", "orientation"],
                fileType: ['jpg', 'png'],
                compressionType: {
                    png: grunt.option('png_compression') || 'losssless',
                    jpg: grunt.option('jpg_compression') || 'lossy',
                    gif: grunt.option('gif_compression') || 'lossless'
                }
            },
            files: [{
                expand: true,
                cwd: 'images/', //specify source folder here, all subfolders within source folder will be kraked and created in destination folder.
                src: ['**/*.{png,jpg,jpeg,gif}'], //file extensions for kraking
                dest: 'imagesKraked/' //desitination folder
            }]
        }
    }
}
```
## Release History
_(Nothing yet)_
